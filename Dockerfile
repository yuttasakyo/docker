FROM node:10.16.0-alpine

LABEL maintainer "Yuttasak (yo) Pannawat"

RUN mkdir /app

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

RUN npm run build

CMD ["npm", "start"]

